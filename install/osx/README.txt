Install ths into your $HOME/Library/LaunchAgents and execute this command:

    launchctl load -w ~/Library/LaunchAgents/gnu.emacs.daemon.plist

You can kill the daemon from inside Emacs with "M-x kill-emacs [RET]".


