;;; just a shim for non-Aquamacs Emacs to make this customization line
;;; pass:
;;;
;;;    '(one-buffer-one-frame-mode nil nil (aquamacs-frame-setup))
;;;

(provide 'aquamacs-frame-setup)
