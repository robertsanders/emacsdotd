;; (add-hook 'after-init-hook
;;           (lambda ()
;;             (if (functionp 'ido-ubiquitous)
;;                 (ido-ubiquitous-mode))))

;; (setq ido-enable-prefix nil
;;       ido-enable-flex-matching t
;;       ido-auto-merge-work-directories-length nil
;;       ido-create-new-buffer 'always
;;       ido-use-filename-at-point 'guess
;;       ido-use-virtual-buffers t
;;       ido-max-prospects 10)


;;
;; http://www.emacswiki.org/emacs/InteractivelyDoThings
;;

;; Display ido results vertically, rather than horizontally
;;    from
;;    http://jaydonnell.com/blog/2011/10/07/setting-up-aquamacs-for-clojure-and-general-goodness/

(setq ido-decorations
      (quote ("\n-> " "" "\n   " "\n   ..." "[" "]" " [No match]"
              " [Matched]" " [Not readable]" " [Too big]" " [Confirm]")))
(defun ido-disable-line-trucation ()
  (set (make-local-variable 'truncate-lines) nil))
(add-hook 'ido-minibuffer-setup-hook 'ido-disable-line-trucation)


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ido-confirm-unique-completion t)
 '(ido-create-new-buffer (quote always))
 '(ido-enable-regexp t)
 '(ido-everywhere nil)
 '(ido-save-directory-list-file "~/.emacs.d/state/.ido.last")
 '(ido-show-dot-for-dired t))
