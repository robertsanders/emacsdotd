(when nil
 (setq linum-format " %d ")

 (progn (defface linum-leading-zero
          `((t :inherit 'linum :foreground
               ,(face-attribute 'linum :background nil t)))
          "Face for displaying leading zeroes for line numbers in display margin." :group 'linum)
        (defun linum-format-func (line)
          (let ((w (length
                    (number-to-string (count-lines (point-min) (point-max))))))
            (concat
             (propertize (make-string (- w (length (number-to-string line))) ?0)
                         'face 'linum-leading-zero)
             (propertize (number-to-string line) 'face 'linum))))
        (setq linum-format 'linum-format-func)))

(message " Loaded linum settings! ")
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(linum-eager t))
