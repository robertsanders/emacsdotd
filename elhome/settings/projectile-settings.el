(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(projectile-ack-function (quote projectile-ack))
 '(projectile-cache-file "/Users/robertsanders/.emacs.d/state/projectile.cache")
 '(projectile-completion-system (quote helm))
 '(projectile-global-mode t))
