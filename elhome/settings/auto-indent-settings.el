(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(auto-indent-after-begin-or-finish-sexp t)
 '(auto-indent-blank-lines-on-move nil)
 '(auto-indent-delete-trailing-whitespace-on-save-file nil)
 '(auto-indent-key-for-end-of-line-insert-char-then-newline "S-M-RET")
 '(auto-indent-key-for-end-of-line-then-newline "M-RET")
 '(auto-indent-kill-remove-extra-spaces t)
 '(auto-indent-known-text-modes (quote (text-mode message-mode fundamental-mode texinfo-mode conf-windows-mode LaTeX-mode latex-mode TeX-mode tex-mode outline-mode markdown-mode nroww-mode deft-mode)))
 '(auto-indent-mode-untabify-on-yank-or-paste nil)
 '(auto-indent-on-yank-or-paste t))
