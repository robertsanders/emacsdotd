
;;;
;;; helm setup
;;;   see
;;; http://amitp.blogspot.com/2012/10/emacs-helm-for-finding-files.html
;;;
;;; https://github.com/emacs-helm/helm/wiki
;;;

(setq helm-idle-delay 0.1)
(setq helm-input-idle-delay 0.1)
(setq helm-ff-lynx-style-map            t
      helm-ff-auto-update-initial-value nil
      helm-yank-symbol-first            t)

(eval-after-load 'helm-files
  '(loop for ext in '("\\.swf$" "\\.elc$" "\\.pyc$")
         do (add-to-list 'helm-boring-file-regexp-list ext)))

(global-set-key (kbd "C-x c h") 'helm-mini)

(add-hook 'helm-after-initialize-hook
          #'(lambda ()
              (define-key helm-buffer-map (kbd "TAB") 'helm-execute-persistent-action)
              (define-key helm-find-files-map (kbd "TAB") 'helm-execute-persistent-action)
              (define-key helm-read-file-map (kbd "TAB") 'helm-execute-persistent-action)))

;; (setq helm-c-locate-command "locate-with-mdfind %.0s %s")

(message "ELHOME loaded helm-settings")

;;;;

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(helm-ff-lynx-style-map nil)
 '(helm-mode t))
