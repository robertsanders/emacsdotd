
;;
;; the clojure-snippets package has a screwy initialization sequence
;; that, if done within the context of a buffer, puts the snippets dir
;; relative to the buffer's directory rather than the packages. ARGH.
;;
(setq clojure-snippets-dir
      (file-name-directory (locate-library "clojure-snippets")))

