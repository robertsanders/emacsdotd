;; get the BCC caching
(rs:load (concat rs:vendor-lisp-directory "byte-code-cache/byte-code-cache"))

;;; this is set higher up in init.el
;; (setq bcc-cache-directory (concat user-emacs-directory "byte-cache/" rs/emacs-variant))
