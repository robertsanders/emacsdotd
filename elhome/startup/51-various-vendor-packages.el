;;
;; Various files in the vendor directory
;;
(setq rs:vendor-init-files '("sudo-save/sudo-save.el"))

;;
;; eager-load some files in the vendor directory
;;
(loop for vendor-file in rs:vendor-init-files
      do (let ((file (concat rs:vendor-lisp-directory vendor-file)))
           (when (file-exists-p file)
             (rs:load file))))

;;
;; Make starter-kit packages available
;;
(loop for starter-kit-dir in
      (directory-files rs:vendor-lisp-directory t "^starter-kit")
      if (file-directory-p starter-kit-dir)
      do (add-to-list 'load-path starter-kit-dir))

(require 'starter-kit-defuns)
