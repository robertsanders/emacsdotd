
Some features of this emacs 

- separate your Emacs config from all the litter files that packages
  produce:
    .emacs-home is where you keep your Emacs code
    .emacs.d    is where Emacs keeps its junk
  
- 
  

Installation
------------



Copy this repo into your Dropbox, and then link to it as:

    cd $HOME && \
      ln -s <DIR_ON_DROPBOX> ~/.emacs-home && \
      mkdir ~/.emacs.d && \
      ln -s ~/.emacs-home/init.el ~/.emacs.d/init.el

Alternately, you can just check it out into your home dir and then
link the "cloud" directory to your Dropbox or other synchronized
storage.

    cd $HOME && \
       git checkout $REPO_URL .emacs-home && \
       ln -s <DIR_ON_DROPBOX> ~/.emacs-home/cloud && \
       mkdir ~/.emacs.d && \
       ln -s ~/.emacs-home/init.el ~/.emacs.d/init.el


