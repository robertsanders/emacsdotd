
;; setup some load paths - override personal-init-directory in your init.el
;; if you prefer

(setq debug-on-error t)

;; setup byte code cache dir
(setq rs/emacs-maker
      (cond
       ((featurep 'aquamacs) 'aquamacs)
       ((featurep 'ns)       'cocoa)
       (t                    'gnu)))

;; allow user to override ~/.emacs-home directory
(setq-default rs:emacs-home-directory
              (replace-regexp-in-string "/*$" "/"
                                        (or (getenv "EMACS_HOME") "~/.emacs-home/")))

(setq-default personal-init-directory (concat rs:emacs-home-directory "personal/"))
(add-to-list 'load-path personal-init-directory)
(setq-default rs:personal-cloud-directory (concat rs:emacs-home-directory "cloud/"))
(setq-default rs:personal-cloud-config-directory (concat rs:personal-cloud-directory "config/"))
(setq-default rs:host-cloud-config-directory 
              (concat rs:personal-cloud-config-directory (car (split-string (system-name) "\\."))))

;; put compat dir at end so it doesn't override any builtin files
(add-to-list 'load-path (concat rs:emacs-home-directory "modules/compat") t)

(add-hook 'after-init-hook
          (lambda ()
            (mapc
             (lambda (dir) (make-directory dir t))
             (list 
              rs:personal-cloud-directory 
              rs:personal-cloud-config-directory rs:host-cloud-config-directory))))

(setq rs/emacs-variant (format "%s-%s" rs/emacs-maker emacs-major-version))
(setq-default bcc-cache-directory (concat user-emacs-directory "byte-cache/" rs/emacs-variant))

;; emacs configuration

; make sure we have homebrew reachable
(push "/usr/local/bin" exec-path)

(defun to-string (string-or-sym)
  "Coerce a symbol to a string, or leave a string as a string"
  (if (symbolp string-or-sym)
      (symbol-name string-or-sym)
    string-or-sym))

(defun concat-stringlike (&rest args)
  "Concatenate a list of stringlike things"
  apply 'concat (mapcar 'to-string args))

(defun personal-init-path (&rest paths)
  (mapconcat 'to-string (cons personal-init-directory paths) "/"))

(defun personal-init-file (&rest paths)
   (concat (apply 'personal-init-path paths) ".el"))

(defun rs:load (file)
  (load (replace-regexp-in-string "\.elc?$" "" file)))

; i'm sure this is defined somewhere
(defun load-init-directory (dir)
  (mapcar
   (lambda (file) (rs:load file))
   (directory-files (personal-init-path dir) t "^[0-9a-zA-Z].*.el$")))


;; allow easy (require) of personal libs 
(add-to-list 'load-path (personal-init-path "libraries") t)

;;;
;;; package.el - Emacs24 default package manager. Don't use this one
;;;              unless necessary; we just init it to fit under el-get
;;;


;; load package.el
(require 'package)

(defadvice package-compute-transaction
  (before
   package-compute-transaction-reverse (package-list requirements)
   activate compile)
  "reverse the requirements"
  (setq requirements (reverse requirements))
  (print requirements))

(defadvice package-download-tar
  (after package-download-tar-initialize activate compile)
  "initialize the package after compilation"
  (package-initialize))

(defadvice package-download-single
  (after package-download-single-initialize activate compile)
  "initialize the package after compilation"
  (package-initialize))

;;;; MELPA - the ELPA bleeding edge
(add-to-list 'package-archives
       '("melpa" . "http://melpa.milkbox.net/packages/") t)

(add-to-list 'package-archives
             '("marmalade" . "http://marmalade-repo.org/packages/") t)
(add-to-list 'package-archives
             '("elpa" . "http://tromey.com/elpa/") t)

(defadvice package-compute-transaction
  (before
   package-compute-transaction-reverse (package-list requirements)
   activate compile)
  "reverse the requirements"
  (setq requirements (reverse requirements))
  (print requirements))

;;;
;;; EL-GET: the main package (meta-)manager I use
;;;
;;;
(setq el-get-dir  (concat user-emacs-directory "el-get/"))
(add-to-list 'load-path (concat el-get-dir "el-get"))

;; So the idea is that you copy/paste this code into your *scratch* buffer,
;; hit C-j, and you have a working developper edition of el-get.
(unless (require 'el-get nil t)
  (url-retrieve
   "https://raw.github.com/dimitri/el-get/master/el-get-install.el"
   (lambda (s)
     (end-of-buffer)
     (eval-print-last-sexp))))

;; So the idea is that you copy/paste this code into your *scratch*
;; buffer, 
;; hit C-j, and you have a working developper edition of el-get.

;; (unless (require 'el-get nil t)
;;   (url-retrieve "https://raw.github.com/dimitri/el-get/master/el-get-install.el"
                ;; (lambda (s) (let (el-get-master-branch) (end-of-buffer) (eval-print-last-sexp)))))

(require 'el-get)

;; HACK - work around eieio version mismatches, I think
(require 'cedet)

;; start up package.el
(package-initialize)

;; my personal recipes and package init files
(add-to-list 'el-get-recipe-path    (personal-init-path "recipes"))
(setq el-get-user-package-directory (personal-init-path "initializers"))

;; load and initialize packages
(el-get 'sync)

