
;; setup some load paths - override personal-init-directory in your init.el
;; if you prefer

(setq debug-on-error 
      (equal (getenv "EMACS_DEBUG_ON_ERROR") "true"))

(cd (expand-file-name "~/"))

;; setup byte code cache dir
(setq rs/emacs-maker
      (cond
       ((featurep 'aquamacs) 'aquamacs)
       ((featurep 'ns)       'cocoa)
       (t                    'gnu)))

;; allow user to override ~/.emacs-home directory
(setq-default rs:emacs-home-directory
              (replace-regexp-in-string "/*$" "/"
                                        (or (getenv "EMACS_HOME") "~/.emacs-home/")))

(setq-default personal-init-directory (concat rs:emacs-home-directory "personal/"))
(add-to-list 'load-path personal-init-directory)
(setq-default rs:personal-cloud-directory (concat rs:emacs-home-directory "cloud/"))
(setq-default rs:personal-cloud-config-directory (concat rs:personal-cloud-directory "config/"))
(setq-default rs:host-cloud-config-directory 
              (concat rs:personal-cloud-config-directory (car (split-string (system-name) "\\."))))

(setq-default rs:vendor-lisp-directory (concat rs:emacs-home-directory "lib/vendor/"))

;; put compat dir at end so it doesn't override any builtin files
(add-to-list 'load-path (concat rs:emacs-home-directory "modules/compat") t)

(defun rs:load-lisp (file)
  (load (replace-regexp-in-string "\.elc?$" "" file)))

(defun rs:load (file)
  (message "Loaded %s: %s"
           file
           (benchmark-run (rs:load-lisp file))))


(add-hook 'after-init-hook
          (lambda ()
            (mapc
             (lambda (dir) (make-directory dir t))
             (list 
              rs:personal-cloud-directory 
              rs:personal-cloud-config-directory rs:host-cloud-config-directory))))

(setq rs/emacs-variant (format "%s-%s" rs/emacs-maker emacs-major-version))
(setq-default bcc-cache-directory (concat user-emacs-directory "byte-cache/" rs/emacs-variant))

(add-hook 'after-init-hook #'(lambda () (message (emacs-init-time))))

;; bootstrap personal-init
(rs:load (concat personal-init-directory "personal-init.el"))

;; load the custom file from our emacs-home
(setq custom-file (concat rs:emacs-home-directory "personal/custom.el"))
(rs:load custom-file)
