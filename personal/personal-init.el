;; emacs configuration

; make sure we have homebrew reachable
(push "/usr/local/bin" exec-path)

(setq byte-compile-verbose nil)
(setq font-lock-verbose nil)

(defun to-string (string-or-sym)
  "Coerce a symbol to a string, or leave a string as a string"
  (if (symbolp string-or-sym)
      (symbol-name string-or-sym)
    string-or-sym))

(defun concat-stringlike (&rest args)
  "Concatenate a list of stringlike things"
  (apply 'concat (mapcar 'to-string args)))

(defun personal-init-path (&rest paths)
  (replace-regexp-in-string "//" "/"
                            (mapconcat 'to-string (cons personal-init-directory paths) "/")))

(defun personal-init-file (&rest paths)
   (concat (apply 'personal-init-path paths) ".el"))

; i'm sure this is defined somewhere
(defun load-init-directory (dir)
  (mapcar
   (lambda (file)
     (with-demoted-errors
       (rs:load file)))
   (directory-files (personal-init-path dir) t "^[0-9a-zA-Z].*.el$")))

(defadvice load-init-directory (around around-benchmark activate)
  "Benchmark loading of init directories"
  (message "Init-directory-loaded %s: %s"
           (ad-get-arg 0)
           (benchmark-run
               ad-do-it)))

; first load the bootstrap files
(load-init-directory "boot")

(unless (require 'idle-require nil t)
  (defalias 'idle-require 'require))

; el-get automatically loads the package initializers in
; "personal/initializers" as needed for each package

; next load the general (not per-user/os/host) configs
(load-init-directory "config")

(mapcar (lambda (file)
          (if (file-exists-p file)
              (with-demoted-errors
                (rs:load file))))
        (list
         (personal-init-file "per-user" user-login-name)
         (personal-init-file "per-os"   system-type)
         (personal-init-file "per-host"
                             (car (split-string system-name "\\.")))))

;; load up the deferred requires
(when (functionp #'idle-require-mode)
  (setq idle-require-idle-delay 5)
  (idle-require-mode 1))
