
;;;; moved to customize
;; (setq make-backup-files nil)
;; (setq auto-save-default nil)
;; (setq-default tab-width 2)
;; (setq-default indent-tabs-mode nil)
;; (setq inhibit-startup-message t)

;; convenience
(fset 'yes-or-no-p 'y-or-n-p)

;; to make ECB happy
(setq stack-trace-on-error nil)

;; we like ispell
;; (require 'ispell)

;; make #! scripts executable after saving them - this is annoying
;; when working in dotto but a good idea otherwise.  if-script-p
;; should look for #!/bin/*sh rather than shell-script-mode or
;; something

;; (add-hook 'after-save-hook 'executable-make-buffer-file-executable-if-script-p)


