;; Enable mouse support
(defun rs/enable-iterm2-mouse-support ()
  "Setup up some basic iterm2 mouse system mappings"
  (interactive)
  (unless window-system
    (require 'mouse)
    (xterm-mouse-mode t)
    (global-set-key [mouse-4] '(lambda ()
                                (interactive)
                                (scroll-down 1)))
    (global-set-key [mouse-5] '(lambda ()
                                (interactive)
                                (scroll-up 1)))
    (defun track-mouse (e))
    (setq mouse-sel-mode t)
    ))

(provide 'iterm2-mouse)
