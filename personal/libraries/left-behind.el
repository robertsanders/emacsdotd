
(defgroup left-behind
  nil
  "Left Behind is a package which helps to sanely share emacs config between hosts \
using a cloud sync service such as Dropbox")

(eval-when-compile 
  (require 'cl))

;; (require 'initsplit)




(provide 'left-behind)
