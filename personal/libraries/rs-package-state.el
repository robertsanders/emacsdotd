;;
;; Save and load a combined package list - supports package.el and el-get
;;
;; Robert Sanders, 2012
;; <robert@curioussquid.com>
;;

(require 'cl)
(require 'finder-inf)

(defgroup rs-package-snapshot nil
  "Saves and restores required package lists")

(defcustom rs:package-snapshot-file
  (concat rs:host-cloud-config-directory "/rs-package-snapshot-file")
  "Save file for rs-package-snapshot package"
  :type 'file
  :group 'rs-package-snapshot)

(defun rs:plist-keys (in-plist)
  "Return a list of keys in IN-PLIST \(conses\)"
  (if (null in-plist)
      in-plist
    (cons (car in-plist) (rs:plist-keys (cddr in-plist)))))

(defun rs:plist-keys-with-value (in-plist value)
  "Return a list of keys in IN-PLIST \(conses\) matching value"
  (rs:plist-keys-matching in-plist (lambda (v) (equal value v))))

(defun rs:plist-keys-matching (in-plist matcher)
  "Return a list of keys in IN-PLIST \(conses\) for which the MATCHER lambda returns a true value"
  (if (null in-plist)
      in-plist
    (let ((result nil))
      (while in-plist
        (if (apply matcher (list (cadr in-plist)))
            (setq result (cons (car in-plist) result)))
        (setq in-plist (cddr in-plist)))
      result)
    ))

(defun rs:alist-keys-with-value (alist value)
  (apply #'append
         (mapcar
          (lambda (pair)
            (if (equal (cdr pair) value)
                (list (car pair))
              nil))
          alist)))

;; (rs:plist-keys-with-value '(:a 12 :b 13 :c 12) 12)
;;    should equal (:c :a)

(defun rs:el-get-package-list ()
  "Return a list of packages currently installed by el-get as a list of strings"
  (rs:alist-keys-with-value (el-get-package-status-alist) "installed"))

(defun rs:package-package-list ()
  "Return a list of packages currently installed by package.el as a list of strings"
  (remove-duplicates
   (append
    (mapcar  (lambda (al) (car al))
              package-alist)
    package-activated-list)))

(defun rs:package-snapshot ()
  "Return an alist of package systems (named with a keyword) and the list of
   packages (as symbols) currently installed"
  (list 
    (cons :el-get    (rs:el-get-package-list))
    (cons :packageel (rs:package-package-list))))

(defun rs:package-snapshot-alist ()
  "Return an alist of package -> source
e.g. ((slime . :el-get) (bbdb . :packageel))"
  (mapcan
   (lambda (pair)
     (let ((pkgsystem (car pair))
           (pkgs      (cdr pair)))
       (mapcar 
        (lambda (pkg)
          (cons pkg pkgsystem))
        pkgs)))
   (rs:package-snapshot)))

;; (rs:installed-package-source 'ibuffer-vc)         -> :packageel
;; (rs:installed-package-source 'ack)                -> :el-get
;; (rs:installed-package-source 'autoconf)           -> :builtin
;; (rs:installed-package-source 'rs-package-state)   -> :other
;; (rs:installed-package-source 'thisaintnopackage)  -> nil

(defun rs:installed-package-source (pkgname)
  "Return the source for an installed package as a keyword, or nil if not installed
Possible return values: [:el-get, :packageel, :builtin, :other, nil]
"
  (cond
   ((assoc pkgname (rs:package-snapshot-alist))
    (cdr (assoc pkgname (rs:package-snapshot-alist))))
   ((assoc pkgname package--builtins) :builtin)
   ((featurep pkgname) :other)
   (t nil)))

(defun rs:save-package-snapshot (&rest args)
  (message "saved R:SPS")
  (with-temp-file rs:package-snapshot-file
    (insert
     (format "%S" (rs:package-snapshot)))))

(defun rs:read-package-snapshot (&optional path)
  (with-temp-buffer
    (insert-file 
     (or path rs:package-snapshot-file))
    (read (buffer-string))
    ))

(defvar rs:last-package-state
      (ignore-errors (rs:read-package-snapshot))
      "Last saved package state for el-get and package.el")

;; utility functions

(defvar rs:local-system-name 
  (car (split-string (system-name) "\\."))
  "Return the name to use as the sync host; defaults to first part of system-name")

;
; Monkey patch some hooks into package.el
;
(defvar rs:package-el-post-install-hooks '()
  "List of hooks to run after package.el installs a new package")
(defvar rs:package-el-post-remove-hooks '()
  "List of hooks to run after package.el removes an installed package")
(defvar rs:package-el-post-update-hooks '()
  "List of hooks to run after package.el updating a package")

(defadvice package-install (after rs:package-hooks activate)
  (message "Running package-post-install-hooks")
  (run-hooks 'rs:package-el-post-install-hooks))

(defadvice package-delete (after rs:package-hooks activate)
  (message "Running package-post-remove-hooks")
  (run-hooks 'rs:package-el-post-remove-hooks))

;
; update the package state after every el-get update
;
(mapcar
 (lambda (hook) (add-hook hook 'rs:save-package-snapshot))
 '(el-get-post-update-hooks
   el-get-post-install-hooks
   el-get-post-remove-hooks
   rs:package-el-post-install-hooks
   rs:package-el-post-remove-hooks
   rs:package-el-post-update-hooks))

;; save package state before exiting
(add-hook 'kill-emacs-hook 'rs:save-package-snapshot)

;;; re-install all packages from fresh

(defun rs:package-bootstrap-el-get (packages)
  (message "Installing el-get packages...")
  (el-get-self-update)
  (el-get-emacswiki-refresh)
  (el-get 'wait packages)
)

(defun rs:package-bootstrap-packageel (packages)
  (message "Installing package.el packages...")
  (let ((package-archives (quote (("ELPA" . "http://tromey.com/elpa/") ("gnu" . "http://elpa.gnu.org/packages/") ("marmalade" . "http://marmalade-repo.org/packages/")))))
    (package-refresh-contents)
    (mapcar
     (lambda (package) (unless (package-installed-p package)
                         (ignore-errors 
                           (package-install package)))
       )
     packages)))


(defun rs:package-bootstrap (sourcefile)
  (message "BOOTSTRAPPING ALL PACKAGES FROM %s" sourcefile)
  (let ((snap (rs:read-package-snapshot sourcefile)))
    (rs:package-bootstrap-packageel (cdr (assoc :packageel snap)))
    (rs:package-bootstrap-el-get (cdr (assoc :el-get snap)))
    )
  (message "All done bootstrapping packages!")
)

(defun rs:package-intersection (needle haystack)
  "Returns a list of all packages from NEEDLE which are in HAYSTACK"
  
  )

(defun rs:packages-installed-from (system)
  (cdr (assoc system (rs:read-package-snapshot))))


(defun rs:available-packages ()
  "Return an alist of available packages"
  (append
   ;; package-el
   (mapcar (lambda (cell) (car cell))
    package-archive-contents)
  ;; el-get
  (el-get-list-package-names-with-status "installed" "available" "removed")))

(defun rs:packages-available-from (system)
  (cdr (assoc system (rs:available-packages))))

(defun rs:packages-movable-to-packageel ()
  "Show a list of packages installed from el-get which could be installed from package.el"
  (intersection
   (rs:packages-installed-from :el-get)
   (package-list-package)
   )
  )

;;; this is how you bootstrap a package list from its snapshot file:
;; (rs:package-bootstrap "~/.emacs-home/cloud/config/hammerhead/rs-package-snapshot-file")

;(cdr (assoc :el-get (rs:package-bootstrap)))
;(cdr (assoc :el-get (rs:package-bootstrap)))

;; ((:el-get el-get color-theme textmate ruby-block ruby-compilation ruby-end rvm maxframe ack rinari rspec-mode ...) (:packageel eproject project clojure-project-mode project-mode levenshtein closure-lint-mode clojure-test-mode clojurescript-mode ecb_snap elisp-depend ibuffer-vc ...))

(provide 'rs-package-state)
