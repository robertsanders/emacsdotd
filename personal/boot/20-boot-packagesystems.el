;;;
;;; package.el - Emacs24 default package manager. Don't use this one
;;;              unless necessary; we just init it to fit under el-get
;;;

(defvar rs:enable-el-get
  (equal (getenv "ENABLE_EL_GET") "true")
  "Whether to enable el-get and load its packages")

;; load package.el
(require 'package)

(defadvice package-compute-transaction
  (before
   package-compute-transaction-reverse (package-list requirements)
   activate compile)
  "reverse the requirements"
  (setq requirements (reverse requirements))
  (print requirements))

(defadvice package-download-tar
  (after package-download-tar-initialize activate compile)
  "initialize the package after compilation"
  (package-initialize))

(defadvice package-download-single
  (after package-download-single-initialize activate compile)
  "initialize the package after compilation"
  (package-initialize))

;;;; MELPA - the ELPA bleeding edge
(add-to-list 'package-archives
       '("melpa" . "http://melpa.milkbox.net/packages/") t)

(add-to-list 'package-archives
             '("marmalade" . "http://marmalade-repo.org/packages/") t)
(add-to-list 'package-archives
             '("elpa" . "http://tromey.com/elpa/") t)

(defadvice package-compute-transaction
  (before
   package-compute-transaction-reverse (package-list requirements)
   activate compile)
  "reverse the requirements"
  (setq requirements (reverse requirements))
  (print requirements))

;;;
;;; EL-GET: the obsolete package (meta-)manager I used to use
;;;
;;;
(when rs:enable-el-get
  (setq el-get-dir "~/.emacs.d/el-get/")
  (add-to-list 'load-path (concat el-get-dir "el-get"))
  
  ;; So the idea is that you copy/paste this code into your *scratch* buffer,
  ;; hit C-j, and you have a working developper edition of el-get.
  (unless (require 'el-get nil t)
    (url-retrieve
     "https://raw.github.com/dimitri/el-get/master/el-get-install.el"
     (lambda (s)
       (end-of-buffer)
       (eval-print-last-sexp))))
  
  ;; So the idea is that you copy/paste this code into your *scratch*
  ;; buffer, 
  ;; hit C-j, and you have a working developper edition of el-get.
  
  ;; (unless (require 'el-get nil t)
  ;;   (url-retrieve "https://raw.github.com/dimitri/el-get/master/el-get-install.el"
  ;; (lambda (s) (let (el-get-master-branch) (end-of-buffer) (eval-print-last-sexp)))))
  
  (require 'el-get))

;; start up package.el
(package-initialize)

(when rs:enable-el-get
  ;; my personal recipes and package init files
  (add-to-list 'el-get-recipe-path    (personal-init-path "recipes"))
  (setq el-get-user-package-directory (personal-init-path "initializers"))

  ;; el-get's support of package.el is a total fucking mess as of 4.1 -
  ;; avoid at all costs
  ;;(add-to-list 'el-get-recipe-path    el-get-recipe-path-elpa t)

  ;; load and initialize packages
  (el-get))

(unless rs:enable-el-get
  (defun el-get (&rest args)
    nil)
  (provide 'el-get))

;;
;; maintain ~/.emacs-home/Carton for package dependencies
;;
(require 'pallet)
(defun pt/carton-file ()
  "Location of the Carton file."
  (personal-init-path "Carton"))
