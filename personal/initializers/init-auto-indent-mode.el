
(add-hook 'ruby-mode-hook         'auto-indent-mode)
(add-hook 'emacs-lisp-mode-hook   'auto-indent-mode)
(add-hook 'shell-script-mode-hook 'auto-indent-mode)
