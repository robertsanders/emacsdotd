(setq rs-anything-inited t)
(require 'anything-config)

(add-to-list 'anything-sources 'anything-c-source-imenu)

(defun anything-emacsdoc ()
  (interactive)
  (anything-other-buffer
   '(anything-c-source-info-pages
     anything-c-source-info-elisp
     anything-c-source-info-cl
     anything-c-source-info-eieio
     anything-c-source-info-emacs
     anything-c-source-man-pages
     anything-c-source-emacs-functions
     anything-c-source-emacs-lisp-toplevels
     anything-c-source-emacs-source-defun
     anything-c-source-emacs-variables
     anything-c-source-emacs-commands)
   " *anything-emacsdoc*"))
