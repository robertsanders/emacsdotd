
(unless (boundp 'aquamacs-version)
  (require 'tabbar)
  (tabbar-mode 1)
  ;; tabbar fwd and back
  (global-set-key [(control tab)]       'tabbar-forward-tab)
  (global-set-key [(control shift tab)] 'tabbar-backward-tab))

