(when (package-installed-p 'tabbar)
  (unless (boundp 'aquamacs-version)
    (eval-after-load 'tabbar
      '(tabbar-mode 1))
    (idle-require 'tabbar)
    
    ;; tabbar fwd and back
    (global-set-key [(control tab)]       'tabbar-forward-tab)
    (global-set-key [(control shift tab)] 'tabbar-backward-tab)))
