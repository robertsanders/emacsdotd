
;; flyspell in clojure mode. Only in comments
(add-hook 'clojure-mode-hook
          (lambda ()
            (flyspell-prog-mode)))

(add-hook 'clojure-mode-hook 'rs:lisp-highlighting t)

(setq clojure-project-extra-classpaths
      '(
        ; "deps/"
        "src/"
        "classes/"
        "test/"
        ))

(setq clojure-project-jar-classpaths
      '(
        ; "deps/"
        "lib/"
        ))

(defun find-clojure-project-jars (path)
  (apply #'append
         (mapcar (lambda (d)
                   (loop for jar in (remove-if (lambda (f) (member f '("." "..")))
                                               (directory-files d t))
                         collect jar into jars
                         finally return jars))
                 (remove-if-not #'file-exists-p
                                clojure-project-jar-classpaths))))

(defun find-clojure-jar (jars)
  (let ((candidates
         (remove-if-not
          (lambda (jar)
            (string-match-p "clojure\\([0-9.-]+\\(SNAPSHOT|MASTER\\)?\\)?\\.jar$" jar))
          jars)))
    (if candidates
        (car candidates)
      (expand-file-name "~/.clojure/clojure.jar"))))

(defun find-clojure-contrib-jar (jars)
  (let ((candidates
         (remove-if-not
          (lambda (jar)
            (string-match-p "clojure-contrib\\([0-9.-]+\\(SNAPSHOT|MASTER\\)?\\)?\\.jar$" jar))
          jars)))
    (if candidates
        (car candidates)
      (expand-file-name "~/.clojure/clojure-contrib.jar"))))

;;; original due to Phil Hagelberg
;;; (see `Best practices for Slime with Clojure' thread on Clojure Google Group)
;; (defun clojure-project (path)
;;   "Sets up classpaths for a clojure project and starts a new SLIME session.

;;    Kills existing SLIME session, if any."
;;   (interactive (list (ido-read-directory-name
;;                       "Project root:"
;;                       (locate-dominating-file default-directory "pom.xml"))))
;;   (when (get-buffer "*inferior-lisp*")
;;     (kill-buffer "*inferior-lisp*"))
;;   (cd path)
;;   ;; I'm not sure if I want to mkdir; doing that would be a problem
;;   ;; if I wanted to open e.g. clojure or clojure-contrib as a project
;;   ;; (both lack "deps/")
;;                                         ; (mapcar (lambda (d) (mkdir d t)) '("deps" "src" "classes" "test"))
;;   (let* ((jars (find-clojure-project-jars path))
;;          (clojure-jar (find-clojure-jar jars))
;;          (clojure-contrib-jar (find-clojure-contrib-jar jars)))
;;     (setq swank-clojure-binary nil
;;           ;; swank-clojure-jar-path (expand-file-name "~/.clojure/clojure.jar")
;;           swank-clojure-jar-path clojure-jar
;;           swank-clojure-extra-classpaths
;;           (cons clojure-contrib-jar
;;                 (append (mapcar (lambda (d) (expand-file-name d path))
;;                                 clojure-project-extra-classpaths)
;;                         (find-clojure-project-jars path)))
;;           swank-clojure-extra-vm-args
;;           (list (format "-Dclojure.compile.path=%s"
;;                         (expand-file-name "classes/" path)))
;;           slime-lisp-implementations
;;           (cons `(clojure ,(swank-clojure-cmd) :init swank-clojure-init)
;;                 (remove-if #'(lambda (x) (eq (car x) 'clojure))
;;                            slime-lisp-implementations))))
;;   (slime))


;;;;
;;;; Auto-completion
;;;;
(require 'hippie-exp)

;;(require 'ac-nrepl)
(autoload 'ac-nrepl-setup "ac-nrepl")
(add-hook 'nrepl-mode-hook             'ac-nrepl-setup)
(add-hook 'nrepl-interaction-mode-hook 'ac-nrepl-setup)
(eval-after-load "auto-complete"
  '(add-to-list 'ac-modes 'nrepl-mode))

(add-hook 'clojure-mode-hook 'auto-complete-mode)

(defun set-auto-complete-as-completion-at-point-function ()
  (setq completion-at-point-functions '(auto-complete)))
(add-hook 'auto-complete-mode-hook 'set-auto-complete-as-completion-at-point-function)

(add-hook 'nrepl-mode-hook 'set-auto-complete-as-completion-at-point-function)
(add-hook 'nrepl-interaction-mode-hook 'set-auto-complete-as-completion-at-point-function)

;;;;
;;;; Automatic docs in nREPL
;;;;
(add-hook 'nrepl-interaction-mode-hook 'nrepl-turn-on-eldoc-mode)
(add-hook 'nrepl-mode-hook             'nrepl-turn-on-eldoc-mode)
