;; We have a number of turn-on-* functions since it's advised that lambda
;; functions not go in hooks. Repeatedly evaluating an add-to-list with a
;; hook value will repeatedly add it since there's no way to ensure
;; that a lambda doesn't already exist in the list.

(defun ch:local-comment-auto-fill ()
  (set (make-local-variable 'comment-auto-fill-only-comments) t)
  (auto-fill-mode t))

(defun ch:turn-on-whitespace ()
  (whitespace-mode +1))

(defun ch:turn-off-whitespace ()
  (whitespace-mode -1))

(defun ch:turn-on-abbrev ()
  (abbrev-mode +1))

(defun ch:turn-on-yasnippet ()
  (when (functionp #'yas-minor-mode-on)
    (yas-minor-mode-on)))

(defun ch:turn-off-abbrev ()
  (abbrev-mode -1))

(defun ch:add-watchwords ()
  (font-lock-add-keywords
   nil '(("\\<\\(FIX\\|TODO\\|FIXME\\|HACK\\|REFACTOR\\|XXX\\):"
          1 font-lock-warning-face t))))

(defun ch:add-keymaps ()
  (local-set-key [(super t)] 'ffip)
  (local-set-key [(super r)] 'helm-imenu))

(defun coding-hook ()
  ;; disabled while getting errors in nxhtml ruby erb editing
  ;;(flyspell-prog-mode)
  (ch:local-comment-auto-fill)
  (when (functionp #'fci-mode)
    ;; fill-column-indicator - causes glitches in minimap-
    (fci-mode))
  (ch:add-keymaps)
  (ch:turn-on-abbrev)
  (ch:turn-on-yasnippet)
  ;; (activate-trailing-whitespace-nazi)
  (ch:add-watchwords))

(require 'flymake)
