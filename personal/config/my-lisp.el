
(ignore-errors
  (require 'starter-kit-lisp-autoloads))

;; (ignore-errors
;;   (require 'hl-sexp)
;;   (require 'highlight-parentheses)
;;   (require 'highline)
;;   (require 'hl-line))

(defun rs:lisp-highlighting ()
  (interactive)
  (if (functionp #'hl-sexp-mode)
      (hl-sexp-mode 1))
  (if (functionp #'highlight-parentheses-mode)
      (highlight-parentheses-mode 1))
  (if (functionp #'rainbow-delimiters-mode)
      (rainbow-delimiters-mode 1))
  (if (functionp #'highline-mode)
      (highline-mode -1))
  (if (functionp #'hl-line-mode)
      (hl-line-mode -1)))

(add-hook 'emacs-lisp-mode-hook 'rs:lisp-highlighting t)
(add-hook 'lisp-mode-hook       'rs:lisp-highlighting t)

(remove-hook 'prog-mode-hook 'esk-turn-on-hl-line-mode)
