(eval-after-load 'yasnippet
  '(progn
     (add-to-list 'yas-snippet-dirs "~/.emacs.d/snippets")
     (add-to-list 'yas-snippet-dirs "~/.emacs-home/snippets")
     (add-to-list 'yas-snippet-dirs "~/.emacs-home/personal/snippets")))

(autoload 'yas-hippie-try-expand "yasnippet")
