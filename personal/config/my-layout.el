
;; setup the window as we like it
; (menu-bar-mode)

; (delete-selection-mode t)
; (scroll-bar-mode -1)
; (tool-bar-mode -1)
; (blink-cursor-mode t)
; (show-paren-mode t)
; (column-number-mode t)
; (set-fringe-style 'default)
; (tooltip-mode)

;; (defvar my-default-font "Bitstream Vera Sans Mono-18"
;;   bitstream vera appears not to have the lambda char for pretty-lambda

(defvar my-default-font "DejaVu Sans Mono-18"
  "The base font and size to use as the base default font")

(defun rs:fontify-frame (frame)
  (set-frame-parameter frame 'font my-default-font))

(defun rs:themify-frame (frame)
  (let ((color-theme-is-global nil))
    (select-frame frame)
    (cond 
     ((display-graphic-p) 
      (color-theme-blackboard))
     (t (color-theme-charcoal-black)))))

(defun rs:frame-adjustment (frame)
  (rs:themify-frame frame)
  (cond
   ((display-graphic-p)
    (rs:fontify-frame frame)
    (rs:maximize-frame frame))))

;; Fontify current frame
(rs:fontify-frame nil)

(defun rs:maximize-frame (frame)
  (maximize-frame frame)
  )

;; Fontify any future frames
(push 'rs:frame-adjustment after-make-frame-functions)

;; (push 'themify-frame after-make-frame-functions)
;; (add-hook 'window-setup-hook 'maximize-frame t)

;; informative thingies in the fringes of the window, like flymake
;; error markers
; (fringe-mode)

;; go big or go home
(add-to-list 'after-init-hook 'maximize-frame)
