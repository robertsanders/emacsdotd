
(when (or (package-installed-p 'rhtml-mode) (functionp #'rhtml-mode)) 
  (autoload 'rhtml-mode "rhtml-mode" nil t)
  (add-to-list 'auto-mode-alist '("\\.erb\\'" . rhtml-mode))
  (add-to-list 'auto-mode-alist '("\\.rjs\\'" . rhtml-mode))
  (eval-after-load 'rhtml-mode
    (add-hook 'rhtml-mode #'(lambda ()
                              (define-key rhtml-mode-map (kbd "M-s") 'save-buffer)))))
