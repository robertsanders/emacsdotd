(when (package-installed-p 'hippie-exp)
  (autoload 'yas/hippie-try-expand "yasnippet")
  (when (package-installed-p 'yasnippet)
    (eval-after-load 'hippie-exp
      '(add-to-list 'hippie-expand-try-functions-list
                    'yas-hippie-try-expand))
    ))

(when (package-installed-p 'smart-tab)
  (idle-require 'smart-tab)
  (eval-after-load 'smart-tab
    '(progn
      (setq smart-tab-using-hippie-expand t)
      (global-smart-tab-mode t))))

