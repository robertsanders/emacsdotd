(when (package-installed-p 'popwin)
 (setq display-buffer-function 'popwin:display-buffer)
  
  ;; (global-set-key (kbd "C-x p") popwin:keymap)
  
  (setq anything-samewindow nil)
  (eval-after-load 'popwin
    '(push '("*anything*" :height 20) popwin:special-display-config)))

;;; https://github.com/emacsmirror/popwin
