(when (package-installed-p 'browse-kill-ring)
  (idle-require 'browse-kill-ring)
  (eval-after-load 'browse-kill-ring
    '(browse-kill-ring-default-keybindings))
  
  ;; this doesn't seem to do anything useful - complains about
  ;; bad arg type integerp
  (global-set-key "\C-cy" #'(lambda ()
                              (interactive)
                              (popup-menu 'yank-menu))))

