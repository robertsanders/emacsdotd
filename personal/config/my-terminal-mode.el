
(when (equal (getenv "TERM_PROGRAM") "iTerm.app")
  (require 'iterm2-mouse)
  (rs/enable-iterm2-mouse-support))
