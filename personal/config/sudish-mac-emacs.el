(when (boundp 'mac-carbon-version-string)
  (defun sj/mac-carbon-toggle-frame-fullscreen ()
    "Make the current frame fullscreen."
    (interactive)
    (let* ((frame (selected-frame))
           (fs-param (if (eq (frame-parameter frame 'fullscreen) 'fullboth)
                         nil
                       'fullboth)))
      (set-frame-parameter frame 'fullscreen fs-param)))
  (define-key global-map [(super return)]
    'sj/mac-carbon-toggle-frame-fullscreen))

;; Distinguish between various Emacs ports to OS X
(cond
 ;; ns port
 ((boundp 'ns-version-string)
  (setq ns-antialias-text t
        ns-option-modifier 'meta)
  (define-key global-map [ns-drag-file] 'ns-find-file))
 ;; mac port
 ((boundp 'mac-carbon-version-string)
  (setq mac-command-modifier 'super
        mac-option-modifier  'meta)
  ;; Command-S to save, C to copy, V to paste, etc.
  ;; (let ((keys '(("\C-x\C-s"    . [(super s)])
  ;;               ("\C-w"        . [(super x)])
  ;;               ("\M-w"        . [(super c)])
  ;;               ("\C-y"        . [(super v)])
  ;;               ([(control /)] . [(super z)]))))
  ;;   (sj/copy-keys-from-keymap global-map keys global-map))
 ))
