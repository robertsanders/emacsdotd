
(when (package-installed-p 'smex)
  (autoload 'smex-major-mode-commands "smex")
  (eval-after-load 'smex
    '(smex-initialize))
  (global-set-key (kbd "M-X") 'smex-major-mode-commands)
  ;; This is your old M-x.
  (global-set-key (kbd "C-c C-c M-x") 'execute-extended-command))
