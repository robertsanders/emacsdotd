(when (package-installed-p 'inf-ruby)
  (add-hook 'ruby-mode-hook 'rs-inf-ruby-hook-function)

  (defun rs-inf-ruby-hook-function ()
    (inf-ruby-setup-keybindings))

  ;; (defvar inf-ruby-implementations
  ;;   '(("ruby"     . "irb --inf-ruby-mode -r irb/completion")
  ;;     ("jruby"    . "jruby -S irb -r irb/completion")
  ;;     ("rubinius" . "rbx -r irb/completion")
  ;;     ("yarv"     . "irb1.9 --inf-ruby-mode -r irb/completion")) ;; TODO: ironruby?
  ;;   "An alist of ruby implementations to irb executable names.")
  
  (eval-after-load 'inf-ruby
    '(add-to-list 'inf-ruby-implementations
                 '("ruby-i" . "irb -f --inf-ruby-mode -r irb/completion")))
  )
