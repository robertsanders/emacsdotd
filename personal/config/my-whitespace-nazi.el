;; zeevex whitespace standards WHETHER YOU LIKE IT OR NOT

(defun activate-trailing-whitespace-nazi ()
  "Annoy you with trailing whitespace, and then delete it on save anyway"
  (interactive)
  (whitespace-mode t)
  (setq show-trailing-whitespace t)
  (setq tab-width 2)
  (setq indent-tabs-mode nil)
  
  (add-hook 'local-write-file-hooks (lambda ()
                                      (untabify (buffer-end 0) (buffer-end 1))
                                      (delete-trailing-whitespace)
                                      nil)))

(provide 'my-whitespace-nazi)
