
(defun is-rails-project ()
  (when (textmate-project-root)
    (file-exists-p (expand-file-name "config/environment.rb" (textmate-project-root)))))

(defun run-rails-test-or-ruby-buffer ()
  (interactive)
  (if (is-rails-project)
      (let* ((path (buffer-file-name))
             (filename (file-name-nondirectory path))
             (test-path (expand-file-name "test" (textmate-project-root)))
             (command (list ruby-compilation-executable "-I" test-path path)))
        (pop-to-buffer (ruby-compilation-do filename command)))
       (ruby-compilation-this-buffer)))

;; hideshow folding
(add-to-list 'hs-special-modes-alist
	     '(ruby-mode
	       "\\(def\\|do\\|{\\)" "\\(end\\|end\\|}\\)" "#"
	       (lambda (arg) (ruby-end-of-block)) nil))

;;; outline-minor-mode folding
;; (add-hook 'ruby-mode-hook
;;               '(lambda ()
;;                  (outline-minor-mode)
;;                  (setq outline-regexp " *\\(def \\|class\\|module\\)")))

;; MuMaMo-Mode for rhtml files
;; (add-to-list 'load-path "~/path/to/your/elisp/nxml-directory/util")

(when (functionp #'eruby-html-mumamo-mode)
   ;; (setq mumamo-chunk-coloring 'submode-colored)
  (add-to-list 'auto-mode-alist '("\\.rhtml\\'"    . eruby-nxhtml-mumamo-mode))
  (add-to-list 'auto-mode-alist '("\\.erb\\'"      . eruby-javascript-mumamo-mode))
  (add-to-list 'auto-mode-alist '("\\.erb.html\\'" . eruby-html-mumamo-mode))
  (add-to-list 'auto-mode-alist '("\\.html.erb\\'" . eruby-html-mumamo-mode)))

;; snippets
;; (require 'yasnippet)


;; (add-to-list 'load-path "~/.emacs.d/manual-packages/rsense/etc")
;; (require 'rsense)
;; (setq rsense-home  "~/.emacs.d/manual-packages/rsense")
;; (setq ruby-program "/Users/robertsanders/.rvm/rubies/ree-1.8.7-2011.03/bin/ruby")
