
;; now set our own packages
;; (setq
;;  my:el-get-packages
;;  '(el-get				; el-get is self-hosting
;;    escreen            			; screen for emacs, C-\ C-h
;;    php-mode-improved			; if you're into php...
;;    switch-window			; takes over C-x o
;;    auto-complete			; complete as you type with overlays
;;    zencoding-mode			; http://www.emacswiki.org/emacs/ZenCoding
;;    color-theme		                ; nice looking emacs
;;    color-theme-tango))	                ; check out color-theme-solarized


;; (global-hl-line-mode)			; highlight current line
;; (global-linum-mode 1)			; add line numbers on the left

;; under mac, have Command as Meta and keep Option for localized input
;; (when (string-match "apple-darwin" system-configuration)
;;   (setq mac-allow-anti-aliasing t)
;;   (setq mac-command-modifier 'meta)
;;   (setq mac-option-modifier 'none))

;; Use the clipboard, pretty please, so that copy/paste "works"
; (setq x-select-enable-clipboard t)


;; winner-mode provides C-<left> to get back to previous window layout
(add-to-list 'after-init-hook
             #'(lambda ()
                 ;; Navigate windows with S-<arrows>
                 (windmove-default-keybindings 'super)
                 (setq windmove-wrap-around nil)
                 (winner-mode 1)))


;; whenever an external process changes a file underneath emacs, and there
;; was no unsaved changes in the corresponding buffer, just revert its
;; content to reflect what's on-disk.
;(global-auto-revert-mode 1)

;; M-x shell is a nice shell interface to use, let's make it colorful.  If
;; you need a terminal emulator rather than just a shell, consider M-x term
;; instead.
(autoload 'ansi-color-for-comint-mode-on "ansi-color" nil t)
(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)

;; If you do use M-x term, you will notice there's line mode that acts like
;; emacs buffers, and there's the default char mode that will send your
;; input char-by-char, so that curses application see each of your key
;; strokes.
;;
;; The default way to toggle between them is C-c C-j and C-c C-k, let's
;; better use just one key to do the same.
;(require 'term)
;(define-key term-raw-map  (kbd "C-'") 'term-line-mode)
;(define-key term-mode-map (kbd "C-'") 'term-char-mode)

;; Have C-y act as usual in term-mode, to avoid C-' C-y C-'
;; Well the real default would be C-c C-j C-y C-c C-k.
;(define-key term-raw-map  (kbd "C-y") 'term-paste)

;; use ido for minibuffer completion
;; (eval-after-load 'ido
;;   (ido-mode t))
;; (idle-require 'ido)

;;(setq ido-save-directory-list-file "~/.emacs.d/.ido.last")
;;(setq ido-enable-flex-matching t)
;;(setq ido-use-filename-at-point 'guess)
;;(setq ido-show-dot-for-dired t)

;; default key to switch buffer is C-x b, but that's not easy enough
;;
;; when you do that, to kill emacs either close its frame from the window
;; manager or do M-x kill-emacs.  Don't need a nice shortcut for a once a
;; week (or day) action.
;(global-set-key (kbd "C-x C-b") 'ido-switch-buffer)
;(global-set-key (kbd "C-x C-c") 'ido-switch-buffer)
;(global-set-key (kbd "C-x B") 'ibuffer)

;; C-x C-j opens dired with the cursor right on the file you're editing
(idle-require 'dired-x)

;; full screen
(defun fullscreen ()
  (interactive)
  (set-frame-parameter nil 'fullscreen
		       (if (frame-parameter nil 'fullscreen) nil 'fullboth)))

(global-set-key [f11] 'fullscreen)
