
;; purty theme

;; (setq color-theme-directory (personal-init-path "color-themes"))
;; (color-theme-initialize)

(when (package-installed-p 'color-theme-approximate)
  (autoload 'color-theme-approximate-on "color-theme-approximate")
  (color-theme-approximate-on))

(when (package-installed-p 'color-theme)
  (load-file (personal-init-path "color-themes/color-theme-blackboard.el"))
  (color-theme-blackboard))



