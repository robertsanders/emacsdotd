
;; i needs me cmd-t
(eval-after-load 'textmate
  (textmate-mode))
(require 'textmate)

;; shut up!
(defun ring-bell-silently ()
  nil)
(setq ring-bell-function 'ring-bell-silently)

;; good old cmd +, cmd - for text site
(global-set-key [(super =)] 'text-scale-adjust)
(global-set-key [(super -)] 'text-scale-adjust)
(global-set-key [(super 0)] 'text-scale-adjust)
(setq text-scale-mode-step 1.1)

;; closing stuff like a mac-boss
(global-set-key [(super w)] 'kill-this-buffer)

;; open me up a line, tm style
(global-set-key [s-return] 'textmate-next-line)

;; sublime/textmate like keys
;;(global-set-key [(super p)] 'helm)
(global-set-key [(super r)] 'helm-imenu)
(global-set-key [(meta R)]  'helm-imenu)

;; prompty M-x
(defun my-execute-extended-command ()
  "Use Helm or SMEX or Anything for extended commands"
  (interactive)
  (cond
   ((functionp #'helm-M-x)     (helm-M-x))
   ((functionp #'smex)         (smex))
   ((functionp #'anything-M-x) (anything-M-x))
   (t                          (execute-extended-command))))

(when (functionp #'helm-M-x)
  (idle-require 'helm))

(global-set-key [(meta x)]         'my-execute-extended-command)
(global-set-key [(super shift p)]  'my-execute-extended-command)

;; override textmate-mode's cmd-t, which isn't really working for me
;; right now
(when (functionp #'helm-cmd-t)
  ;; cmd-t
  (global-set-key                 [(super t)] 'helm-cmd-t)

  ;; shift-alt-T
  (global-set-key                 [(meta T)]  'helm-cmd-t)

  (when (functionp #'textmate-mode)
    (eval-after-load 'textmate
      '(define-key *textmate-mode-map* [(super t)] 'helm-cmd-t))))

(defun rs:find-file-in-git ()
  (interactive)
  (cond
   ((functionp #'helm-ls-git-ls)       (command-execute 'helm-ls-git-ls))
   ((functionp #'helm-cmd-t)           (command-execute 'helm-cmd-t))
   ((functionp #'projectile-find-file) (command-execute 'projectile-find-file))
   (t                                  (command-execute 'find-file))))

(defun rs:find-file-dwimmly ()
  (interactive)
  (cond
   ((functionp #'helm-find-files)      (command-execute 'helm-for-files))
   ((functionp #'ido-find-file)        (command-execute 'ido-find-file))
   (t                                  (command-execute 'find-file))))

(defun rs:in-git-project-p ()
  (interactive)
  (vc-git-root (buffer-file-name)))

(defun rs:find-file-textmately ()
  (interactive)
  (if (rs:in-git-project-p)
      (rs:find-file-in-git)
    (rs:find-file-dwimmly)))

(global-set-key                 [(super p)] 'rs:find-file-textmately)
(global-set-key                 [(meta P)]  'rs:find-file-textmately)

(global-set-key                 "\C-xcf" 'helm-for-files)
(eval-after-load 'helm-files
  '(progn
     (require 'helm-ls-git)
     (add-to-list 'helm-for-files-preferred-list 'helm-source-ls-git t)))

;;
;; fix bug in M-s-] parsing on Cocoa Emacs 23/24 on Lion
;; apparently in pure meta-[ or meta-] it works okay, but super-meta-[ and super-meta-] get
;; seen as super-(altgr [) and super-(altgr ])
;;

(global-set-key [(meta super ?})] 'align)

;; see steveyegge -
;; https://sites.google.com/site/steveyegge2/effective-emacs - I want
;; my kill-region cake too

(defun kill-region-or-backward-word (arg)
  (interactive "p")
  (if (region-active-p)
      (kill-region (region-beginning) (region-end))
    (backward-kill-word arg)))
(global-set-key [(ctrl w)] 'kill-region-or-backward-word)

(if (featurep 'ns)
    (eval-after-load 'textmate
      '(ignore-errors
	 (define-key *textmate-mode-map* [(meta super ?†)]      'textmate-clear-cache)      ; super-meta-t
	 (define-key *textmate-mode-map* [(meta super ?\u8224)] 'textmate-clear-cache)      ; super-meta-t
	 (define-key *textmate-mode-map* [(meta super ?“)] 'indent-according-to-mode)       ; super-meta-[
	 (define-key *textmate-mode-map* [(meta super ?\u8220)] 'indent-according-to-mode)  ; super-meta-[
	 (define-key *textmate-mode-map* [(meta super ?‘)] 'align) ; super-meta-]
	 (define-key *textmate-mode-map* [(meta super ?\u8216)] 'align))))

(when (featurep 'aquamacs)
  (global-set-key [(control tab)]       'next-tab-or-buffer)
  (global-set-key [(control shift tab)] 'previous-tab-or-buffer))

;; easy m-x access per stevey
(global-set-key "\C-c\C-m"         'my-execute-extended-command)

;; show ibuffer, sorted by VC root
(global-set-key "\C-x\C-b" 'ibuffer)

(add-hook 'after-init-hook 
	  (lambda ()
       (add-hook 'ibuffer-hook 'ibuffer-vc-set-filter-groups-by-vc-root)))

;; -*- coding: utf-8 -*-
