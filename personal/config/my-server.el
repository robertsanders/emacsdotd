;; start emacs server if we don't have one

(ignore-errors
  (require 'server)
  (unless (server-running-p) (server-start)))
