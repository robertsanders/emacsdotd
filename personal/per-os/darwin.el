;;
;; http://www.emacswiki.org/emacs/MacOSTweaks
;; http://www.emacswiki.org/emacs/EmacsForMacOS
;;


(setq-default ispell-program-name "/usr/local/bin/aspell")

; OSX ls doesn't support "--dired"
(setq dired-use-ls-dired nil)

; don't do that hold-for-accent thing. didn't need this in Homebrew
; Emacs 24, but it can't hurt.
(when (and (featurep 'ns) (window-system))
  (ns-set-resource nil "ApplePressAndHoldEnabled" "NO")
  ;; make wheel scrolling less jerky
  (setq mouse-wheel-scroll-amount '(0.01)))

; delete files by moving them to the OS X trash
(setq delete-by-moving-to-trash t)

; pick up changes to files on disk automatically (ie, after git pull)
(global-auto-revert-mode 1)

; buffer switching
(global-set-key [(super {)] 'previous-buffer)
(global-set-key [(super })] 'next-buffer)

; run Ruby tests, TextMate-style
(add-hook 'rinari-minor-mode-hook
  (lambda ()
    (define-key rinari-minor-mode-map (kbd "s-r") 'rinari-test)))

;; colorful shell
;;(require 'ansi-color)

(autoload 'ansi-color-for-comint-mode-on "ansi-color" nil t)
(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)

(setenv "PAGER" "cat")

(idle-require 'osx-plist-editing)
