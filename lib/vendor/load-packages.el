;;;
;;; package.el - Emacs24 default package manager. Don't use this one
;;;              unless necessary; we just init it to fit under el-get
;;;


;; load package.el
(require 'package)

;;;; MELPA - the ELPA bleeding edge
(add-to-list 'package-archives
       '("melpa" . "http://melpa.milkbox.net/packages/") t)

(add-to-list 'package-archives
             '("marmalade" . "http://marmalade-repo.org/packages/") t)
(add-to-list 'package-archives
             '("elpa" . "http://tromey.com/elpa/") t)

;;;
;;; EL-GET: the main package (meta-)manager I use
;;;
;;;
(setq el-get-dir "~/.emacs.d/el-get/")
(add-to-list 'load-path (concat el-get-dir "el-get"))

;; So the idea is that you copy/paste this code into your *scratch* buffer,
;; hit C-j, and you have a working developper edition of el-get.
(unless (require 'el-get nil t)
  (url-retrieve
   "https://raw.github.com/dimitri/el-get/master/el-get-install.el"
   (lambda (s)
     (end-of-buffer)
     (eval-print-last-sexp))))

;; So the idea is that you copy/paste this code into your *scratch*
;; buffer, 
;; hit C-j, and you have a working developper edition of el-get.

;; (unless (require 'el-get nil t)
;;   (url-retrieve "https://raw.github.com/dimitri/el-get/master/el-get-install.el"
                ;; (lambda (s) (let (el-get-master-branch) (end-of-buffer) (eval-print-last-sexp)))))

(require 'el-get)

;; HACK - work around eieio version mismatches, I think
; (require 'cedet)

;; start up package.el
(package-initialize)

;; load and initialize packages
(el-get)

